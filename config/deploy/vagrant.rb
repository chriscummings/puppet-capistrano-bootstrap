ip = '33.33.33.33'
role :app, ip
role :web, ip
role :db,  ip, :primary => true

# Use vagrant's SSH key
set :ssh_options, {:forward_agent => true, :keys => ['~/.vagrant.d/insecure_private_key']}

set :rails_env, 'production'