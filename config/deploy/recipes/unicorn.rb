namespace :unicorn do

  task :up, :roles => :app do
    output("Starting unicorn in #{rails_env + ' environment'} now...")
    run "cd #{current_path}; bundle exec unicorn -c config/unicorn.rb -D -E #{rails_env}"
  end

  task :down, :roles => :app do
    output('Stopping unicorn now...')
    run "kill -s QUIT `cat /tmp/unicorn.#{application}.pid`"
  end

  task :bounce do
          unicorn.stop
          unicorn.start
  end

end