namespace :puppet do

	task :install do
		set :user, "root"
		set :use_sudo, true
		set :default_shell, "bash"
		
		# Change settings for Vagrant VM
		if (fetch :stage) == :vagrant
			# Don't use user root if vagrant
			set :user, "vagrant" 
			# Use vagrant's SSH key
			set :ssh_options, {:forward_agent => true, :keys => ['~/.vagrant.d/insecure_private_key']}
		end

		# Update the server
		try_sudo("apt-get update -y")

		# Upgrade server and supress interactivity
		try_sudo("DEBIAN_FRONTEND=noninteractive apt-get upgrade -y")

		# Install puppet and supress interactivity
		try_sudo("DEBIAN_FRONTEND=noninteractive apt-get install puppet -y")
	end

	task :update do
		set :user, "root"
		set :use_sudo, true
		set :default_shell, "bash"

		# Change settings for Vagrant VM
 		if (fetch :stage) == :vagrant
			# Don't use user root if vagrant
			set :user, "vagrant" 
			# Use vagrant's SSH key
			set :ssh_options, {:forward_agent => true, :keys => ['~/.vagrant.d/insecure_private_key']}
		end

		# Compress puppet files
		system("tar czf 'puppet.tgz' puppet/")

		# Upload to server
		upload("puppet.tgz","/tmp",:via => :scp)

		# Uncompress
		try_sudo("tar -C /tmp -xzf /tmp/puppet.tgz")

		# Remove existing puppet stuff
		try_sudo("rm -rf /etc/puppet/manifests")
		try_sudo("rm -rf /etc/puppet/modules")
		try_sudo("rm -rf /etc/puppet/puppet.conf")

		# Replace puppet stuff
		try_sudo("mv /tmp/puppet/manifests /etc/puppet/manifests")
		try_sudo("mv /tmp/puppet/modules /etc/puppet/modules")
		try_sudo("mv /tmp/puppet/puppet.conf /etc/puppet/puppet.conf")

		# Run puppet apply and supress interactivity
		try_sudo("DEBIAN_FRONTEND=noninteractive puppet apply --verbose --debug /etc/puppet/manifests/site.pp")
	end

	task :bootstrap do
		puppet.install
		puppet.update
	end
end