staging_ip = '162.209.106.107'

role :web,    staging_ip
role :app,    staging_ip
role :db,     staging_ip, :primary => true
role :worker, staging_ip

set :rails_env, "development"