require "bundler/capistrano"
require 'capistrano/ext/multistage'
require "rvm/capistrano"

load 'config/deploy/recipes/puppet'
load 'config/deploy/recipes/helpers'
load 'config/deploy/recipes/unicorn'

# Network
ssh_options[:forward_agent] = true

# Multistage
set :stages, ['vagrant', 'staging']
set :default_stage, 'vagrant'

set :rvm_type, :system
