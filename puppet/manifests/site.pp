Exec {
	path => "/usr/local/sbin:/usr/local/bin:/user/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
}

include basics
include iptables
include monit
include mysql
include nginx
include rails_app
include redis
include ruby
include rvm
include ssh
include users
