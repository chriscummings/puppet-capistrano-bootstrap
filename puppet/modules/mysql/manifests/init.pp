class mysql {

    # Ensure MySQL is installed. 
    package {
		"mysql-server":
			ensure => present
    }

    file {
	    "/etc/mysql/my.cnf":
            require => Package["mysql-server"],
            source  => "puppet:///modules/mysql/my.cnf",
            path    => '/etc/mysql/my.cnf',
            owner   => 'root',
            group   => 'root',
            mode    => 644
    }

    service {
        "mysql":
            ensure => true,
            enable => true,
            require => File["/etc/mysql/my.cnf"],
            subscribe => File["/etc/mysql/my.cnf"]
    }

    exec {
    	# Change password
	    "mysql_password":
	        unless  => "mysqladmin -uroot -proot sasha_production",
	        command => "mysqladmin -uroot password root",
	        require => Service["mysql"];

    	# Create application DB
        "sasha_db":
            unless  => "mysql -uroot -proot sasha_production",
            command => "mysql -uroot -proot -e 'create database sasha_production'",
            require => Exec["mysql_password"]
    }

}