# References: 
# http://docs.puppetlabs.com/references/stable/type.html#user
# https://groups.google.com/forum/#!topic/puppet-users/OG0Q7h68O9I

class users {

	# Create my chris (admin) account.
	user {
		"chris":
			groups     => 'sudo',
			ensure     => present,
			managehome => true,
			shell      => "/bin/bash",
			password   => '$1$42VTG9K0$7M3E0sILa1e0wqARrb8CF1' # temporary, encrypted password. openssl passwd -1
	}

	# Create chris' .ssh folder
	file {
		"/home/chris/.ssh":
			ensure  => directory,
			require => User['chris'],
			owner   => 'chris',
			mode    => 700
	}

	# Add chris' public ssh key
	ssh_authorized_key {
		"chris-rsa-key":
			ensure => present,
			key    => "AAAAB3NzaC1yc2EAAAADAQABAAABAQCvFw7C2g1TzlXGDi5SWmYMK5JMZTcIV6uijv2vNpSyydlvngcClgThdu1hls/eE4nmzANIiPqJlzaTNudabu0KMPRJMcYu1V1/Oa0SDH0q9w9D/vMZn1/DBgaWK/O0Wuy5LmOL5yYVZV92DAmEK4Z+IpbqQZb/Wv/xFfaNE3Mg27ALbu0A2+Cng/I0AtrZ10PhlzZm+vrxFkRWFa7IMhgvTc4Rhtgdod8wBrBwxFooju36GnSTKfFIBusXtYWAymGOuSbNqMV/Nca0ZjFHNrfJw0uWvS6jZWAA/9NBOgBqNpiapM3+/rn8c1Z8630XkVDmees0QBlZC6r2ApI449zF",
			type   => rsa,
			user   => 'chris',
			require => File["/home/chris/.ssh"]
	}

	# Create my app_user account.
	user {
		"app_user":
			groups     => '',
			ensure     => present,
			managehome => true,
			shell      => "/bin/bash",
			password   => '$1$42VTG9K0$7M3E0sILa1e0wqARrb8CF1' # temporary, encrypted password. openssl passwd -1
	}

	# Create app_user's .ssh folder
	file {
		"/home/app_user/.ssh":
			ensure  => directory,
			require => User['app_user'],
			owner   => 'app_user',
			mode    => 700
	}

	# Add app_user's public ssh key
	ssh_authorized_key {
		"app_user-rsa-key":
			ensure => present,
			key    => "AAAAB3NzaC1yc2EAAAADAQABAAABAQCvFw7C2g1TzlXGDi5SWmYMK5JMZTcIV6uijv2vNpSyydlvngcClgThdu1hls/eE4nmzANIiPqJlzaTNudabu0KMPRJMcYu1V1/Oa0SDH0q9w9D/vMZn1/DBgaWK/O0Wuy5LmOL5yYVZV92DAmEK4Z+IpbqQZb/Wv/xFfaNE3Mg27ALbu0A2+Cng/I0AtrZ10PhlzZm+vrxFkRWFa7IMhgvTc4Rhtgdod8wBrBwxFooju36GnSTKfFIBusXtYWAymGOuSbNqMV/Nca0ZjFHNrfJw0uWvS6jZWAA/9NBOgBqNpiapM3+/rn8c1Z8630XkVDmees0QBlZC6r2ApI449zF",
			type   => rsa,
			user   => 'app_user',
			require => File["/home/app_user/.ssh"]
	}

}