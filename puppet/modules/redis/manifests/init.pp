class redis {

	# Ensure redis is installed
	package {
		"redis-server":
			ensure => present
	}

	# Backup redis config file
	exec {
		"backup redis config":
			require => Package["redis-server"],
			command => "sudo cp /etc/redis/redis.conf /etc/redis/redis.conf.default"
	}

	# Ensure redis-server is installed and running.
	service {
		"redis-server":
			ensure => true,
			enable => true
	}
	
}
