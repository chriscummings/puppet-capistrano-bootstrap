class monit {

	# Ensure monit is installed. 
	package {
		"monit":
			ensure => present
	}

	# Backup monit config file
	exec {
		"backup monit config":
			require => Package["monit"],
			command => "sudo cp /etc/monit/monitrc /etc/monit/_monitrc"
	}

	# Upload custom monit config file.
	file {
		"/etc/monit/monitrc":
			require => Exec["backup monit config"],
			source  => "puppet:///modules/monit/monitrc",
			path    => '/etc/monit/monitrc',
			owner   => 'root',
			group   => 'root',
			mode    => 700
	}

	# Reload config
	exec {
		"reload config":
			require => File["/etc/monit/monitrc"],
			command => "sudo /etc/init.d/monit reload"
	}

}