class ruby {

    # Depends on RVM, install ruby 1.9.3 & set as default
    rvm_system_ruby {
        'ruby-1.9.3-p448':
            ensure => 'present',
            default_use => true
    }

    rvm_gem {
      'ruby-1.9.3-p448/bundler':
          require => Rvm_system_ruby['ruby-1.9.3-p448'],
          ensure => latest
    }

    # Dependencies for the mysql2 gem. 
    $mysql2_gem_depedencies = ['libmysql-ruby', 'libmysqlclient-dev']
    package {
        $mysql2_gem_depedencies:
            ensure => present
    }

}