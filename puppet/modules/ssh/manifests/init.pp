class ssh {

	# Ensure SSH is installed and running.
	service {
		"ssh":
			ensure => true,
			enable => true
	}

	# Upload custom ssh config file.
	file {
		"/etc/ssh/sshd_config":
			source  => "puppet:///modules/ssh/sshd_config",
			path    => '/etc/ssh/sshd_config',
			owner   => 'root',
			group   => 'root',
			mode    => 444,
			notify  => Service["ssh"]
	}

}