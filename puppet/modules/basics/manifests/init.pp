class basics {

	# Ensure unattended-upgrades is installed (automatic security upgrades). 
	package {
		"unattended-upgrades":
			ensure => present
	}

}