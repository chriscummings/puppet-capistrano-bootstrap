class nginx {

	# Ensure nginx is installed
	package {
		"nginx":
			ensure => present
	}

	# Ensure nginx is installed and running.
	service {
		"nginx":
			ensure => true,
			enable => true
	}

	# Backup nginx default config file
	exec {
		"backup nginx default config":
			require => Package["nginx"],
			command => "sudo cp /etc/nginx/sites-available/default /etc/nginx/sites-available/_default"
	}
	
}