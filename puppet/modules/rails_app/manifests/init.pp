class rails_app {

	# Create application folder
	file { "/var/www":
	    ensure => "directory",
	    owner  => "app_user"
	}

	# Remove symlink to default nginx config
	file { "/etc/nginx/sites-enabled/default":
		require => Package["nginx"],	
	    ensure => "absent"
	}

	# Upload nginx config for rails application
	file {
		"rails_app_vhost_file":
			require => Package["nginx"],
			source  => "puppet:///modules/rails_app/nginx/vhost_files/rails_app",
			path    => '/etc/nginx/sites-available/rails_app'
	}

	# Symlink nginx rails config to enabled dir
	file {
		"/etc/nginx/sites-enabled/rails_app":
			require => File["rails_app_vhost_file"],	
			ensure => 'link',
			target  => '/etc/nginx/sites-available/rails_app'
	}

    # Reload nginx
    # There isn't a "reload" option for the pupper service type so we use exec.
    exec {
	    "reload nginx":
            require => File["/etc/nginx/sites-enabled/rails_app"],
            command => "sudo /etc/init.d/nginx reload"
    }

}