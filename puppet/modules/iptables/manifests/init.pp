class iptables {

	# Upload custom iptables config file.
	file {
		"/etc/network/iptables":
			# See note below regarding the ".suspect_bad_lines" version of 
			# this file.
			source  => "puppet:///modules/iptables/iptables",
			path    => '/etc/network/iptables.suspect_bad_lines',
			owner   => 'root',
			group   => 'root',
			mode    => 444,
	}

	# Fix EOF errors in iptables config file.
	exec {
		"fix any lines or EOF borked by non-unix text editors":

			# I kept running into the following error when attempting to restore 
			# iptable rules: "iptables-restore v1.x.x: no command specified"
			# with this file after I had worked on the file in Textmate. The 
			# "fix" for this was removing empty lines from within nano or alike.
			#
			# For the life of me I could not get sed to fix this issue and grep
			# won't do an inline replace, thus the "suspect_bad_lines" version.

			command => "grep -v '^$' /etc/network/iptables.suspect_bad_lines > /etc/network/iptables",
			require => File["/etc/network/iptables"]	
	}

	# Upload iptables import bash script.
	file {
		"/etc/network/if-pre-up.d/update-iptables":
			require => File["/etc/network/iptables"],
			source  => "puppet:///modules/iptables/update-iptables",
			path    => '/etc/network/if-pre-up.d/update-iptables',
			owner   => 'root',
			group   => 'root',
			mode    => 444
	}

	# Make iptables import bash script executable.
	exec {
		"make update-iptables executable":
			command => "chmod a+x /etc/network/if-pre-up.d/update-iptables",
			require => File["/etc/network/if-pre-up.d/update-iptables"]	
	}

	# Run iptables import bash script.
	exec {
		"update iptables":
			command => "/etc/network/if-pre-up.d/update-iptables | sh",
			require => Exec["make update-iptables executable"]	
	}

}