ToDo
====

* Use erb templates:
 * Deal w/username and pass in monit/files/monitrc file under httpd.
 * Deal w/root pass in mysql's manifest.
 * Should create a mysql specific user for the app & set permissions for it in mysql's manifest.
 * Deal with creating application DB in mysql's manifest.

* SSH - b/c with the way it is now, locking things down prevents futher updates...
 * Not currently locking out root
 * Not preventing password auth


Adding a Server/Stage
=====================

* Create a file (`<some_stage>.rb`) in `config/deploy/`
* Add capistrano variables:

		<some_stage>_ip = '<Server IP address>'

		role :web,    <some_stage>_ip
		role :app,    <some_stage>_ip
		role :db,     <some_stage>_ip, :primary => true
		role :worker, <some_stage>_ip

		set :rails_env, '<some_environment>'

* Add the stage to the stages list in `config/deploy.rb`:

		set :stages, ['vagrant', 'some_stage']


Set up VirtualBox
==================

*(This only needs to be done once. This avoids the lengthy process of getting ubuntu up to date every time you start a new virtual machine.)*

* Install [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
* Install [Vagrant](http://downloads.vagrantup.com/)
* Get a Ubuntu 12.04 LTS box:

		vagrant box add precise64 http://files.vagrantup.com/precise64.box

* Create a temporary folder and change directory to it.

		mkdir temp; cd temp

* Run initiate vagrant within that folder

		vagrant init

* Edit the new `Vagrantfile`:

		# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
		VAGRANTFILE_API_VERSION = "2"
	
		Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
			config.vm.box = 'precise64'
		end

	
* Start the virtual machine: 
 
		vagrant up

* SSH into the virtual machine: 
 
		vagrant ssh

* Update the server:

		sudo apt-get update -y
		sudo apt-get upgrade -y


* Exit from the SSH session:

		exit

* Package the box and shut down the virtual machine:

		vagrant package
		vagrant box add precise64_updated package.box
		vagrant destroy


Bootstrap you server(s)
=======================

*(This will take a brand new provisioned server and install puppet, update the OS, install dependencies and configure it.*

*This runs both `puppet:install` and `upper:update` rake tasks.)*

* Either move your project files into this this directory or copy/merge repo files into your project.
* Edit `config/deploy.rb` as needed.
* Create and update stage files in `config/deploy/` as needed.

**Vagrant virtual machine**

		vagrant up
		cap vagrant puppet:bootstrap

**Remote server**

		cap <stage> puppet:bootstrap


